//
//  Person.swift
//  BHTT
//
//  Created by Danny Thüring on 27/05/15.
//  Copyright (c) 2015 Danny Thuering. All rights reserved.
//

class Person {
  var firstName = ""
  var lastName = ""
  var email = ""
  var description = ""
  
  init() {}
  
  init(firstName:String, lastName:String) {
    self.firstName = firstName
    self.lastName = lastName
  }
  
  func getFullName() -> String {
    return "\(firstName) \(lastName)"
  }
  
}
