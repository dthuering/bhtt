//
//  ListView.swift
//  BHTT
//
//  Created by Danny Thüring on 27/05/15.
//  Copyright (c) 2015 Danny Thuering. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Swift_RAC_Macros

class ListViewModel: NSObject {
  
  var person = Person()
  
  let personList = [
    Person(firstName: "Christine", lastName: "Perpetua"),
    Person(firstName: "Benjamin", lastName: "Fajr"),
    Person(firstName: "Xanthe", lastName: "Marshall"),
    Person(firstName: "Ina", lastName: "Artur"),
    Person(firstName: "Elli", lastName: "Eutropia"),
    Person(firstName: "Yisra'el", lastName: "Geraint"),
    Person(firstName: "Cecilia", lastName: "Saniyya"),
    Person(firstName: "Lidija", lastName: "Nkiruka"),
    Person(firstName: "Sierra", lastName: "Marika"),
    Person(firstName: "Leanne", lastName: "Laz"),
    Person(firstName: "Simon", lastName: "Darian"),
    Person(firstName: "Roland", lastName: "Duncan")
  ]
  
  var searchText:String? = nil
  
  override init() {
    
  }
  
  init(p:Person) {

    self.person = p
  }

  func getPersonNames() -> [String] {
    var thePersons = personList
    
    if let theSearchText = searchText {
      thePersons = personList.filter { (p:Person) -> Bool in
        return p.getFullName().lowercaseString.rangeOfString(theSearchText.lowercaseString) != nil
      }
    }
    
    return thePersons.map { (p:Person) -> String in p.getFullName() }
  }
  
}
