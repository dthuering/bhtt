//
//  ViewController.swift
//  BHTT
//
//  Created by Danny Thüring on 26/05/15.
//  Copyright (c) 2015 Danny Thuering. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Swift_RAC_Macros

class EntryViewController: UIViewController {

  @IBOutlet weak var errorLabel: UILabel!

  @IBOutlet weak var firstNameText: UITextField!
  @IBOutlet weak var lastNameText: UITextField!
  @IBOutlet weak var emailText: UITextField!
  @IBOutlet weak var descriptionText: UITextView!
  
  @IBOutlet weak var signupBtn: UIButton!

  @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
  
  let descriptionPlaceholder = "please write something about yourself"
  
  let viewModel = EntryViewModel()
  
  override func viewDidLoad() {
		super.viewDidLoad()

    firstNameText.rac_textSignal() ~> RAC(viewModel, "firstName")
    lastNameText.rac_textSignal() ~> RAC(viewModel, "lastName")
    emailText.rac_textSignal() ~> RAC(viewModel, "email")
    descriptionText.rac_textSignal() ~> RAC(viewModel, "pdescription")
    
    descriptionText.rac_textSignal().subscribeNext { (next:AnyObject!) -> Void in
      self.descriptionHeightConstraint.constant = self.descriptionText.contentSize.height
    }
    
    RAC(self.signupBtn, "enabled") <~ RACObserve(viewModel, "isSignupEnabled")
    
    RAC(self.errorLabel, "text") <~ RACObserve(viewModel, "validationText")
    
    signupBtn.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext { (next:AnyObject!) -> Void in
      let alertCtrl = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
      alertCtrl.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.Default, handler: { action in
        self.performSegueWithIdentifier("gotoListViewSegue", sender: nil)
      }))
      alertCtrl.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
      
      self.presentViewController(alertCtrl, animated: true, completion: nil)
    }
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden", name: UIKeyboardDidHideNotification, object: nil)

  }

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let vc = segue.destinationViewController as! ListViewController
    vc.viewModel = ListViewModel(p:viewModel.person)
  }
  
  func keyboardWillShow(aNotification:NSNotification) {
    let info: NSDictionary = aNotification.userInfo!
    let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as! NSValue
    let keyboardSize: CGSize = value.CGRectValue().size
    self.bottomSpaceConstraint.constant = keyboardSize.height + 8
  }
  
  func keyboardWillBeHidden() {
    bottomSpaceConstraint.constant = 8
  }
  
  func doneEditDescription() {
    descriptionText.resignFirstResponder()
  }

}

extension EntryViewController : UITextViewDelegate {

  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    if(descriptionText.text == descriptionPlaceholder) {
      descriptionText.text = ""
    }
    descriptionText.textColor = UIColor.blackColor()

    let b = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "doneEditDescription")
    self.navigationItem.rightBarButtonItem = b
    
    return true
  }
  
  func textViewShouldEndEditing(textView: UITextView) -> Bool {
    if(descriptionText.text == "") {
      descriptionText.text = descriptionPlaceholder
      descriptionText.textColor = UIColor.lightGrayColor()
    }
    self.navigationItem.rightBarButtonItems = []
    return true
  }
  
}

