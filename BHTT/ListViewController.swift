//
//  ListViewController.swift
//  BHTT
//
//  Created by Danny Thüring on 27/05/15.
//  Copyright (c) 2015 Danny Thuering. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
  
  @IBOutlet weak var personTableView: UITableView!
  @IBOutlet weak var navigationBar: UINavigationBar!
  @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
  
  var searchController = UISearchController(searchResultsController: nil)
  
  var viewModel:ListViewModel = ListViewModel()
  
  var searchText:String? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()

    navigationBar.topItem?.title = "\(viewModel.person.firstName) \(viewModel.person.lastName)"

    searchController.dimsBackgroundDuringPresentation = false
    searchController.delegate = self
    searchController.searchBar.sizeToFit()
    searchController.searchBar.delegate = self
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden", name: UIKeyboardDidHideNotification, object: nil)
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func searchClick(sender: AnyObject) {
    personTableView.tableHeaderView = searchController.searchBar
    personTableView.reloadData()
  }

  func keyboardWillShow(aNotification:NSNotification) {
    let info: NSDictionary = aNotification.userInfo!
    let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as! NSValue
    let keyboardSize: CGSize = value.CGRectValue().size
    self.tableViewBottomConstraint.constant = keyboardSize.height
  }
  
  func keyboardWillBeHidden() {
    tableViewBottomConstraint.constant = 0
  }
  
}

extension ListViewController : UITableViewDataSource {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.getPersonNames().count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
    cell.textLabel?.text = viewModel.getPersonNames()[indexPath.row]
    return cell
  }
  
}

extension ListViewController : UITableViewDelegate {
  
}

extension ListViewController : UISearchControllerDelegate {
  
  func willDismissSearchController(searchController: UISearchController) {
    personTableView.tableHeaderView = nil
    viewModel.searchText = nil
    personTableView.reloadData()
  }
  
}

extension ListViewController : UISearchBarDelegate {
  
  func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
    viewModel.searchText = searchText
    if count(searchText) == 0 {
      viewModel.searchText = nil
    }
    personTableView.reloadData()
  }

}

