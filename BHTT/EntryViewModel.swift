//
//  EntryView.swift
//  BHTT
//
//  Created by Danny Thüring on 27/05/15.
//  Copyright (c) 2015 Danny Thuering. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Swift_RAC_Macros

class EntryViewModel : NSObject {

  dynamic var firstName = ""
  dynamic var lastName = ""
  dynamic var email = ""
  dynamic var pdescription = ""
  
  dynamic var isSignupEnabled = false
  
  var validationNames = ""
  var validationEmail = ""
  
  dynamic var validationText = ""
  
  let person = Person()

  override init() {
    super.init()
    
    let firstNameSignal = RACObserve(self, "firstName")

    let lastNameSignal = RACObserve(self, "lastName")

    let validNamesSignal = RACSignal.combineLatest([firstNameSignal, lastNameSignal]).map { (next:AnyObject!) -> AnyObject! in
      let fn = next.first() as! String
      let ln = next.last as! String
      
      self.validationNames = ""
      
      let validFirstName = self.checkValidLength(fn)
      let validLastName = self.checkValidLength(ln)
      let equalNames = fn == ln
      
      if(!validFirstName) { self.validationNames = "First Name not valid;" }
      if(!validLastName) { self.validationNames = "\(self.validationNames) Last Name not valid;" }
      if(equalNames) { self.validationNames = "\(self.validationNames) Names should not match;" }
      
      return !equalNames && validFirstName && validLastName
    }

    let validEmailSignal = RACObserve(self, "email").map { (next:AnyObject!) -> AnyObject! in
      let isValidEmail = self.isValidEmail(next as! String)

      self.validationEmail = ""
      if(!isValidEmail) { self.validationEmail = "Not an valid eMail" }

      return isValidEmail
    }

    let validInputsSignal = RACSignal.combineLatest([validNamesSignal, validEmailSignal]).map { (next:AnyObject!) -> AnyObject! in
      self.validationText = "\(self.validationNames) \(self.validationEmail)"
      return next.first() as! Bool && next.last as! Bool
    }

    validInputsSignal.subscribeNext { (next:AnyObject!) -> Void in
      self.isSignupEnabled = next as! Bool
      if(self.isSignupEnabled) {
        self.person.firstName = self.firstName
        self.person.lastName = self.lastName
        self.person.email = self.email
      }
    }

    let descriptionSignal = RACObserve(self, "pdescription").subscribeNext { (next:AnyObject!) -> Void in
      self.person.description = next as! String
    }
    
  }
  
  func checkValidLength(text:String) -> Bool {
    return count(text) > 2
  }
  
  func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluateWithObject(testStr)
  }

}

