//
//  BHTTTests.swift
//  BHTTTests
//
//  Created by Danny Thüring on 26/05/15.
//  Copyright (c) 2015 Danny Thuering. All rights reserved.
//

import Quick
import Nimble
import ReactiveCocoa
import Swift_RAC_Macros

class EntryViewModelTests: QuickSpec {

  override func spec() {
    
    describe("the name validator") {
      it("should reject lengths less than 3") {
        let vm = EntryViewModel()
        expect(vm.checkValidLength("")).to(equal(false))
        expect(vm.checkValidLength("22")).to(equal(false))
        expect(vm.checkValidLength("333")).to(equal(true))
      }
    }
    
    describe("the email validator") {
      it("should reject invalid emails") {
        let vm = EntryViewModel()
        expect(vm.isValidEmail("")).to(equal(false))
        expect(vm.isValidEmail("123")).to(equal(false))
        expect(vm.isValidEmail("danny@")).to(equal(false))
        expect(vm.isValidEmail("@.de")).to(equal(false))
        expect(vm.isValidEmail("danny@de")).to(equal(false))
        expect(vm.isValidEmail("danny@.de")).to(equal(false))
        expect(vm.isValidEmail("danny@test.de")).to(equal(true))
      }
    }

    describe("the model") {
      let vm = EntryViewModel()
      var isSignupEnabled = false
      
      beforeEach {
        RACObserve(vm, "isSignupEnabled").subscribeNext({ (next:AnyObject!) -> Void in
          isSignupEnabled = next.boolValue
        })
        
        vm.firstName = "123"
        vm.lastName = "456"
        vm.email = "danny@inm.de"
      }
      
      it("should send a signal when validates true") {
        expect(isSignupEnabled).to(equal(true))
      }
    }

    describe("the model") {
      let vm = EntryViewModel()
      var validationText = ""
      
      beforeEach {
        RACObserve(vm, "validationText").subscribeNext({ (next:AnyObject!) -> Void in
          validationText = next as! String
        })
        
        vm.firstName = ""
        vm.lastName = ""
        vm.email = ""
      }
      
      it("should show all validation errors") {
        expect(validationText).to(contain("First"))
        expect(validationText).to(contain("Last"))
        expect(validationText).to(contain("eMail"))
      }
    }
    
    describe("the model") {
      let vm = EntryViewModel()
      var validationText = ""
      
      beforeEach {
        RACObserve(vm, "validationText").subscribeNext({ (next:AnyObject!) -> Void in
          validationText = next as! String
        })
        
        vm.firstName = "123"
        vm.lastName = ""
        vm.email = ""
      }
      
      it("should show validation errors") {
        expect(validationText).notTo(contain("First"))
        expect(validationText).to(contain("Last"))
        expect(validationText).to(contain("eMail"))
      }
    }
    
    describe("the model") {
      let vm = EntryViewModel()
      var validationText = ""
      
      beforeEach {
        RACObserve(vm, "validationText").subscribeNext({ (next:AnyObject!) -> Void in
          validationText = next as! String
        })
        
        vm.firstName = "123"
        vm.lastName = "123"
        vm.email = ""
      }
      
      it("should show validation errors") {
        expect(validationText).notTo(contain("First"))
        expect(validationText).notTo(contain("Last"))
        expect(validationText).to(contain("match"))
        expect(validationText).to(contain("eMail"))
      }
    }
    
    describe("the model") {
      let vm = EntryViewModel()
      var validationText = ""
      
      beforeEach {
        RACObserve(vm, "validationText").subscribeNext({ (next:AnyObject!) -> Void in
          validationText = next as! String
        })
        
        vm.firstName = "123"
        vm.lastName = "456"
        vm.email = ""
      }
      
      it("should show validation errors") {
        expect(validationText).notTo(contain("First"))
        expect(validationText).notTo(contain("Last"))
        expect(validationText).notTo(contain("match"))
        expect(validationText).to(contain("eMail"))
      }
    }
    
    describe("the model") {
      let vm = EntryViewModel()
      var validationText = ""
      
      beforeEach {
        RACObserve(vm, "validationText").subscribeNext({ (next:AnyObject!) -> Void in
          validationText = next as! String
        })
        
        vm.firstName = "123"
        vm.lastName = "456"
        vm.email = "d@d.de"
      }
      
      it("should show no validation errors") {
        expect(validationText).notTo(contain("First"))
        expect(validationText).notTo(contain("Last"))
        expect(validationText).notTo(contain("match"))
        expect(validationText).notTo(contain("eMail"))
      }
    }

    describe("the validated data") {
      let vm = EntryViewModel()
      
      beforeEach {
        vm.firstName = "123"
        vm.lastName = "456"
        vm.email = "d@d.de"
        vm.pdescription = "description"
      }
      
      it("should contain a Person object containing the validated data") {
        let p = vm.person
        expect(p.firstName).to(equal("123"))
        expect(p.lastName).to(equal("456"))
        expect(p.email).to(equal("d@d.de"))
        expect(p.description).to(equal("description"))
      }
    }

  }
  
}
