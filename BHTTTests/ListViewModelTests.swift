//
//  ListViewModelTests.swift
//  BHTT
//
//  Created by Danny Thüring on 31/05/15.
//  Copyright (c) 2015 Danny Thuering. All rights reserved.
//

import Quick
import Nimble

class ListViewModelTests: QuickSpec {
  
  override func spec() {
    
    describe("search trough data") {
      
      it("should return the correct results") {
        let vm = ListViewModel()
        
        vm.searchText = "li"
        let result1 = vm.getPersonNames()
        expect(result1.count).to(equal(3))

        vm.searchText = "xxx"
        let result2 = vm.getPersonNames()
        expect(result2.count).to(equal(0))

        vm.searchText = "tu"
        let result3 = vm.getPersonNames()
        expect(result3.count).to(equal(2))

      }
      
    }
    
  }
  
}